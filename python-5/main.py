from datetime import datetime

LIMIT_DAYTIME = 6
LIMIT_NIGHTTIME = 22
DAYTIME_TAX = 0.09
NIGHTTIME_TAX = 0
CHARGE = 0.36

records = [
    {'source': '48-996355555', 'destination': '48-666666666', 'end': 1564610974, 'start': 1564610674},
    {'source': '41-885633788', 'destination': '41-886383097', 'end': 1564506121, 'start': 1564504821},
    {'source': '48-996383697', 'destination': '41-886383097', 'end': 1564630198, 'start': 1564629838},
    {'source': '48-999999999', 'destination': '41-885633788', 'end': 1564697158, 'start': 1564696258},
    {'source': '41-833333333', 'destination': '41-885633788', 'end': 1564707276, 'start': 1564704317},
    {'source': '41-886383097', 'destination': '48-996384099', 'end': 1564505621, 'start': 1564504821},
    {'source': '48-999999999', 'destination': '48-996383697', 'end': 1564505721, 'start': 1564504821},
    {'source': '41-885633788', 'destination': '48-996384099', 'end': 1564505721, 'start': 1564504821},
    {'source': '48-996355555', 'destination': '48-996383697', 'end': 1564505821, 'start': 1564504821},
    {'source': '48-999999999', 'destination': '41-886383097', 'end': 1564610750, 'start': 1564610150},
    {'source': '48-996383697', 'destination': '41-885633788', 'end': 1564505021, 'start': 1564504821},
    {'source': '48-996383697', 'destination': '41-885633788', 'end': 1564627800, 'start': 1564626000}
]


# função acumula os valores de ligação para cada número de telefone e ordena por valor de ligação:
def sum_vl_calls(calls):
    dict_sum_calls = {}
    list_final_calls = []
    total_vl_call = 0
    pre_source = calls[0]['source']

    for call in calls:

        # acumula enquanto os valores de source forem iguais
        if pre_source == call['source']:
            pre_source = call['source']
            total_vl_call += call['total']
        # quando for diferente, grava
        else:
            dict_sum_calls = {'source': pre_source, 'total': round(total_vl_call, 2)}
            list_final_calls.append(dict_sum_calls)
            pre_source = call['source']
            total_vl_call = call['total']

    # grava o ultimo elemento lido na lista:
    dict_sum_calls = {'source': pre_source, 'total': round(total_vl_call, 2)}
    list_final_calls.append(dict_sum_calls)

    # ordena por valor total
    list_final_calls = sorted(list_final_calls, key=lambda k: k['total'], reverse=True)

    return list_final_calls


# função classifica as ligações por número de telefone e calcula o preço por ligação:
def classify_by_phone_number(records):
    # ordena os registros do dicionário pela chave source:
    sorted_records = sorted(records, key=lambda k: k['source'])

    tax = 0
    list_calls = []

    i = 0

    for record in sorted_records:

        i += 1
        # converte hora start / end de formato unix para datetime:
        start_conv = datetime.fromtimestamp(record['start'])
        end_conv = datetime.fromtimestamp(record['end'])
        duration = end_conv - start_conv
        duration_sec = duration.seconds // 60

        # ligação começou no período diurno
        if LIMIT_DAYTIME <= start_conv.hour < LIMIT_NIGHTTIME:
            # ligação terminou no período diurno
            if LIMIT_DAYTIME <= end_conv.hour:
                tax = DAYTIME_TAX
            # ligação terminou no período noturno
            elif LIMIT_NIGHTTIME <= end_conv.hour:
                dif_duration = LIMIT_NIGHTTIME - start_conv.hour
                duration_sec = dif_duration
        # ligação começou no período noturno
        else:
            tax = NIGHTTIME_TAX

        # calcula valor da ligação e zera a taxa:
        vl_call = (CHARGE + (tax * duration_sec))
        tax = 0

        # print(f"{i} source: {record['source']}, start: {start_conv}, end: {end_conv}, "
        #      f"duration: {duration}, duration sec: {duration_sec}, vl: {vl_call}")

        dict_calls = {'source': record['source'], 'total': vl_call}
        list_calls.append(dict_calls)

    # chama função que acumula os valores das ligações por telefone:
    output_list = sum_vl_calls(list_calls)
    print(output_list)

    return output_list

classify_by_phone_number(records)
