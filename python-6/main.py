# Pessoal, ainda estou tentando entender conceitos x pŕatica de orientação a objeto,
# então pra mim é confuso entender tudo isso...
# Está cheio de comentários porque peguei os passos do desafio e fui comentando no código
# o que tentei fazer, mas já peço desculpas porque sei que não fica muito legível (rs)

from abc import ABC, abstractmethod

DEFAULT_HOURS = 8
EXTRA_BONUS = 0.15


class Department:
    def __init__(self, name, code):
        self.name = name
        self.code = code


# Proteja a classe `Employee` para não ser instânciada diretamente.
# biblioteca abc (abstract base class)
class Employee(ABC):
    def __init__(self, code, name, salary, department):
        self.code = code
        self.name = name
        self.salary = salary

        # Proteja o atributo `department` da classe `Manager` para que seja acessado
        # somente através do método `get_department`.
        self.__department = department

    # Torne obrigatório a implementação dos métodos da classe `Employee`, implemente-os
    # se for necessários.
    @abstractmethod
    def calc_bonus(self):
        pass

    def get_hours(self):
        # Padronize uma carga horária de 8 horas para todos os funcionários.
        return DEFAULT_HOURS

    # Implemente o método `get_department` que retorna o nome do departamento e
    # `set_department` que muda o nome do departamento para as classes `Manager` e `Seller`
    def set_department(self, department):
        # Proteja o atributo `department` da classe `Manager` para que seja acessado
        # somente através do método `get_department`.
        self.__department = department

    def get_department(self):
        # Proteja o atributo `department` da classe `Manager` para que seja acessado
        # somente através do método `get_department`.
        return self.__department.name


# Faça a correção dos métodos para que a herança funcione corretamente.
class Manager(Employee):
    def __init__(self, code, name, salary):
        # Faça a correção dos métodos para que a herança funcione corretamente.
        super().__init__(code, name, salary, Department('managers', 1))

    def calc_bonus(self):
        return self.salary * EXTRA_BONUS


class Seller(Employee):
    def __init__(self, code, name, salary):
        # Faça a correção dos métodos para que a herança funcione corretamente.
        super().__init__(code, name, salary, Department('sellers', 2))
        # Proteja o atributo sales da classe Seller para que não seja acessado diretamente
        self.__sales = 0

    # O cálculo do metodo `calc_bonus` do Vendedor dever ser calculado pelo
    # total de suas vendas vezes 0.15
    def calc_bonus(self):
        return self.get_sales() * EXTRA_BONUS

    # crie um método chamado `get_sales` para retornar o valor do atributo
    # e `put_sales` para acrescentar valores a esse atributo, lembrando
    # que as vendas são acumulativas
    def get_sales(self):
        return self.__sales

    def put_sales(self, value):
        self.__sales += value