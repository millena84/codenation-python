from main import get_temperature
import main
from unittest.mock import patch
import pytest

''' SETUP: Valores padronizados para simulação da chamada da API e cálculo '''
default_test_values = [
    (65.34, -14.235004, -51.92528, 18),
    (73.44, 45.4626482, 9.037647, 23),
    (80.91, 43.7799368, 11.1709276, 27),
    (71.82, 45.4042007, 12.1071435, 22)
]


@pytest.mark.parametrize("far,lat,lon,expected", default_test_values)
def test_get_temperature_by_lat_lng(far, lat, lon, expected):
    """ SETUP: Definição do Json do mock da requisição na API """
    data = {
        "latitude": lat,
        "longitude": lon,
        "currently": {
            "temperature": far
        }
    }

    """ EXECUÇÃO: chamada da API mockado """

    """ - Indica onde a simulação da requisição está sendo feita (programa main.py) """
    mock_req = patch('main.requests.get')
    """ - Inicia a simulação """
    mock_simu_get = mock_req.start()
    """ - Recupera o campo do json definido como retorno da simulação (data) """
    mock_simu_get.return_value.json.return_value = data
    """ - Simula chamada da função """
    resp = get_temperature(lat, lon)
    """ - Finaliza simulação """
    mock_req.stop()

    """ VERIFICAÇÃO: definição do resultado esperado """
    assert resp == expected


@patch('main.requests.get')
def test_request_api_none(mock_class):
    mock_class.return_value.json.return_value = {"currently": {"temperature": None}}
    response = get_temperature(0, 0)
    assert None == response
