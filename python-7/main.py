import requests
from requests.exceptions import RequestException


KEY = 'e1ee55658d4a2b28c4841e373c3b3d87'


def get_temperature(lat, lng):

    url = 'https://api.darksky.net/forecast/{}/{},{}'.format(KEY, lat, lng)

    response = requests.get(url)
    data = response.json()
    temperature = data.get('currently').get('temperature')

    if not temperature:
        return
    return int((temperature - 32) * 5.0 / 9.0)


def request_api(lat, lon):

    url = 'https://api.darksky.net/forecast/{}/{},{}'.format(KEY, lat, lon)

    try:
        response = requests.get(url)
        return response.status_code

    except RequestException:
        print(RequestException)
        return

