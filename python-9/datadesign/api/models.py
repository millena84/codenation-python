from django.core.validators import validate_email, MinLengthValidator, validate_ipv4_address
from django.db import models


class User(models.Model):
    name = models.CharField('Nome', max_length=50)
    last_login = models.DateTimeField('Último login', auto_now_add=True)
    email = models.EmailField('E-mail', validators=[validate_email])
    password = models.CharField('Senha',
                                max_length=50,
                                validators=[MinLengthValidator(8)])

    def __str__(self):
        return self.name


class Agent(models.Model):
    name = models.CharField('Nome', max_length=50)
    status = models.BooleanField('Status', default=False)
    env = models.CharField('Ambiente', max_length=20)
    version = models.CharField('Versão', max_length=5)
    address = models.GenericIPAddressField('Endereço',
                                           max_length=39,
                                           protocol='both',
                                           validators=[validate_ipv4_address])

    def __str__(self):
        return self.name


class Event(models.Model):
    LEVEL_CHOICES = [
        ("C", "Critical"),
        ("D", "Debug"),
        ("E", "Error"),
        ("W", "Warning"),
        ("I", "Info")
    ]

    level = models.CharField('Nível', choices=LEVEL_CHOICES, max_length=20)
    data = models.TextField('Dados')
    arquivado = models.BooleanField('Arquivado', default=False)
    date = models.DateField('Data', auto_now_add=True)
    agent = models.ForeignKey(
        Agent,
        on_delete=models.deletion.CASCADE
    )
    user = models.ForeignKey(
        User,
        on_delete=models.deletion.CASCADE
    )

    def __str__(self):
        return self.level


class Group(models.Model):
    name = models.CharField('Nome', max_length=50)

    def __str__(self):
        return self.name


class GroupUser(models.Model):
    group = models.ForeignKey(
        Group,
        on_delete=models.deletion.CASCADE
    )
    user = models.ForeignKey(
        User,
        on_delete=models.deletion.CASCADE
    )