from collections import Counter

from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.


@api_view(['POST', 'GET'])
def lambda_function(request):
    try:
        if request.method == "POST":
            request_data = request.data.get('question')

            counter = Counter()

            for number in request_data:
                counter[number] += 1

            solution = []

            for item in counter.most_common():
                for i in range(item[1]):
                    solution.append((item[0]))

            return Response({"solution": solution})
        else:
            return Response({"retorno": "Só é permitido verbo POST nessa rota"})
    except:
        return Response({"retorno": 'Rota espera formato {"question":[1,3,9,6,9]}'},
                        status=status.HTTP_400_BAD_REQUEST)


