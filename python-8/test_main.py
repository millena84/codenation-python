import jwt
from main import create_token
from main import verify_signature
from jwt import exceptions

# Complete a funções para rertornar um json web token, gerado usando o algoritmo HS256, que contenha a seguinte informação **{"language": "Python"}**,
#  com a palavra secreta **acelera**, complete a função para decifrar as informações tratando a exceção de quando a assinatura for inválida
#  devendo retornar o seguinte dicionário {"error": 2}.
#  Não havendo erro, retornar a informação decifrada.


class TestChallenge4:

    # base 64: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYW5ndWFnZSI6IlB5dGhvbiJ9.X4QXKREUTxVVH0HMmXB0Obe4YVYpQsEMvC1P4vYQLp0
    TOKEN = b'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYW5ndWFnZSI6IlB5dGhvbiJ9.sM_VQuKZe_VTlqfS3FlAm8XLFhgvQQLk2kkRTpiXq7M'
    DATA = {"language": "Python"}
    SECRET = "acelera"

    def test_create_token(self):
        assert create_token(self.DATA, self.SECRET) == self.TOKEN

    def test_verify_signature(self):
        assert verify_signature(self.TOKEN) == self.DATA

    def test_verify_signature_invalid_signature_error(self):
        assert verify_signature('.') == {"error": 2}

    def test_verify_signature_decode_error(self):
        assert verify_signature('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJx.'
                                'eyJsYW5ndWFnZSI6IlB5dGhvbiJ9.'
                                'sM_VQuKZe_VTlqfS3FlAm8XLFhgvQQLk2kkRTpiXq7M') == {'error': 2}
