import jwt

def create_token(data, secret):

    jwt_format = jwt.encode(data, secret, algorithm='HS256')

    return jwt_format


def verify_signature(token):

    try:
        payload_format = jwt.decode(token, 'acelera', algorithm='HS256')
    except jwt.InvalidTokenError:
        return {'error': 2}
    except jwt.DecodeError:
        raise {'error': 2}

    return payload_format

